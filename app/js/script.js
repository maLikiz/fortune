$(document).ready(function(){
	var	pointsArr = [0,250,100,625,0,500,1500,750],
		textEmpty = '<div>Попробуй<br>еще</div>';

	var $drum = $('.drum'),
		$drumBtn = $('.drum-btn');

		$EldrumPart = '<div class="drum-part"></div>'
		for (i = 0; i <= pointsArr.length-1; i++) {
			$drum.append($EldrumPart)
		}
		var $drumPart = $('.drum-part')
		for (i = 0; i <= pointsArr.length-1; i++) {
			if (pointsArr[i] == 0) {
				$drumPart.eq(i).css({
					'font-size': 16
				}).html(textEmpty)
			} else {
				$drumPart.eq(i).html(pointsArr[i])
			}
			var dpRotate = i*45,
				dpLeft = 0,
				dpRight = 0,
				dpTop = 0,
				dpBottom = 0;

			if (dpRotate == 0) {
				dpBottom = 'auto'
			}
			if (dpRotate == 180) {
				dpTop = 'auto'
			}
			if (dpRotate > 0 && dpRotate < 180) {
				dpLeft = 'auto'
			}
			if (dpRotate > 180 && dpRotate < 360) {
				dpRight = 'auto'
			}

			$drumPart.eq(i).css({
				'transform':'rotate('+dpRotate+'deg)',
				'left': dpLeft,
				'right': dpRight,
				'top': dpTop,
				'bottom': dpBottom
			})
		}

	function getRotationDegrees(obj) {
	    var matrix =
	    obj.css("-webkit-transform") ||
	    obj.css("-moz-transform")    ||
	    obj.css("-ms-transform")     ||
	    obj.css("-o-transform")      ||
	    obj.css("transform");
	    if(matrix !== 'none') {
	        var values = matrix.split('(')[1].split(')')[0].split(','),
	        	a = values[0],
	        	b = values[1],
	        	angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
	    } else { var angle = 0; }
	    return (angle < 0) ? angle + 360 : angle;
	    console.log(angle)
	}
	canClick = true
	function result() {
		var oldRotate = getRotationDegrees($drum)
		$drum.removeAttr('style')
		$drum.css({
			'transform':'rotate('+(oldRotate)+'deg)'
		})

		var i = Math.floor(Math.random() * (pointsArr.length - 1)) + 0,
			// rotate = 45*i+360*10,
			rotate = 45*i+360*15,
			// duration = 1000,
			duration = 16000,
			// cb = 'cubic-bezier(0, 1, 0, 1)';
			cb = 'cubic-bezier(0, 0.25, 0, 1)';
		setTimeout(function() {
			$drum.css({
				'transition': duration+'ms '+cb+' transform',
				'transform':'rotate('+(-rotate)+'deg)'
			})
		}, 10);
		canClick = false
		setTimeout(function() {
			canClick = true
			modalShow()
		}, duration);
		console.log(pointsArr[i])
	}
	$drumBtn.click(function(){
		if (canClick == true) {
			result()
			bigArrowHide()
		}
	})


	// стрелка
	$bigArrow = $('.big-arrow')
	$coverWin = $('.cover-win')
	$popupWin = $('.popup-win')
	function bigArrowShow() {
		$bigArrow.addClass('show')

	}
	function bigArrowHide() {
		$bigArrow.removeClass('show').css({
			'transition':'.5s'
		})
	}
	setTimeout(function() {
		bigArrowShow()
	}, 700);

	
	// модалка
	function modalShow() {
		var speed = 300
		$coverWin.addClass('show')
		$coverWin.css({
			'transition':speed+'ms'
		});
		setTimeout(function() {
			$coverWin.css({
				'opacity':1
			})
			$popupWin.addClass('show')
		}, 10);
	}
	function modalHide() {
		var speed = 300
		$coverWin.css({
			'transition':speed+'ms',
			'opacity':0
		})
		setTimeout(function() {
			$popupWin.removeClass('show')
			$coverWin.removeAttr('style').removeClass('show')
		}, speed);
	}
	$coverWin.on('click', '.btn', function() {
		modalHide()
	})
})