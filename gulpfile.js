var gulp         = require('gulp'),
	sass         = require('gulp-sass')
	browserSync  = require('browser-sync'),
	concat       = require('gulp-concat'),
	uglify       = require('gulp-uglifyjs'),
	cssnano      = require('gulp-cssnano'),
	rename       = require('gulp-rename'),
	del          = require('del'),
	imagemin     = require('gulp-imagemin'),
	pngquant     = require('imagemin-pngquant'),
	cache        = require('gulp-cache'),
	autoprefixer = require('gulp-autoprefixer'),
    fileinclude  = require('gulp-file-include'),
    sourcemaps   = require('gulp-sourcemaps'),
    plumber      = require('gulp-plumber'),
	htmlreplace  = require('gulp-html-replace');
function swallowError (error) {
  // If you want details of the error in the console
  console.log(error.toString())
  this.emit('end')
}

gulp.task('fileinclude', function() {
	gulp.src(['app/pages/index.html'])
		.pipe(fileinclude({
			prefix: '@@',
			basepath: 'app'
		}))
		// .pipe(concat('index.html'))
		.pipe(gulp.dest('app'))
		.pipe(browserSync.reload({stream: true}))

	// gulp.src(['app/pages/watch.html'])
	// 	.pipe(fileinclude({
	// 		prefix: '@@',
	// 		basepath: 'app'
	// 	}))
	// 	.pipe(gulp.dest('app'))
	// 	.pipe(browserSync.reload({stream: true}))
});
gulp.task('sass', function(){
	return gulp.src('app/sass/style.sass')
		.pipe(plumber())
		.pipe(sourcemaps.init({
			loadMaps: true,
			largeFile: true
		})) //
		.pipe(sass({errLogToConsole: true}))
		.pipe(autoprefixer({
			browsers: ['last 15 versions', '> 1%', 'ie 8', 'ie 7'],
			cascade: true
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.reload({stream: true}))
})
gulp.task('scripts-libs', function() {
	gulp.src([
		'app/libs/jquery-3.1.1.min.js',
		// 'app/libs/jquery-ui.min.js',
		// 'app/libs/owl.carousel.js',
		// 'app/libs/jquery.maskedinput.js',
		// 'app/libs/jquery.mask.js',
		// 'app/libs/jquery.tablesorter.min.js',
		// 'app/libs/jquery.scrollbar.js',
		// 'app/libs/jquery.ui.touch-punch.min.js',
		// 'app/libs/jquery.floatThead.js',
		// 'app/libs/jquery.form.js',
		// 'app/libs/jquery.transform.js',
		// 'app/libs/sketch.js'
	])
	.pipe(concat('libs.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('app/js'));
})
gulp.task('scripts', function() {
	gulp.src([
		'app/js/script.js'
	])
	.pipe(concat('bundle.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('dist/js'));
})
gulp.task('css-libs', ['sass'],function() {
	return gulp.src('app/css/style.css')
	.pipe(cssnano())
	.pipe(rename({suffix: '.min'}))
	.on('error', swallowError)
	.pipe(gulp.dest('app/css'));
});
gulp.task('browser-sync',function(){
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false,
		ghostMode: {
          scroll: false
        }
	});
});

gulp.task('cleanHTML', function(){
	return del.sync('app/*.html');
});
gulp.task('clear', function(){
	return cache.clearAll();
});

gulp.task('img', function(){
	return gulp.src('app/img/**/*')
		.pipe(cache(imagemin({
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		})))
		.pipe(gulp.dest('dist/img'));
});

gulp.task('include-watch', ['fileinclude']);

gulp.task('watch', [
	'cleanHTML',
	'browser-sync',
	// 'scripts',
	'include-watch'

	], function(){
		gulp.watch('app/**/*.html', ['include-watch'], browserSync.reload);
		gulp.watch('app/**/*.svg', ['include-watch'], browserSync.reload);
		gulp.watch('app/sass/*.sass', ['sass']);
		gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('default', function() {
  gulp.src('app/*.html')
    .pipe(htmlreplace({
        'css': 'css/style.min.css?6',
        'js': 'js/bundle.min.js?6'
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('build', [
	// 'clean', 
	// 'img', 
	'css-libs',
	'sass',
	'scripts-libs',
	'scripts',
	'fileinclude',
	'default'],	function() {

	var buildCss = gulp.src([
		'app/css/style.min.css'
		])
		.pipe(gulp.dest('dist/css'));

	var buldFonts = gulp.src('app/fonts/**/*')
		.pipe(gulp.dest('dist/fonts'));

	var buldVideo = gulp.src('app/video/**/*')
		.pipe(gulp.dest('dist/video'));

	var buildJs = gulp.src('app/js/**/*.min.js')
		.pipe(gulp.dest('dist/js'));

	// var buildHtml = gulp.src('app/index.html')
	// 	.pipe(gulp.dest('dist'));

});

gulp.task('wb',function(){
	browserSync({
		server: {
			baseDir: 'dist'
		},
		notify: false
	});
});